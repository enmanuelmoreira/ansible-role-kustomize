# Ansible Role: Kustomize

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kustomize/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kustomize/-/commits/main)

This role installs [Kustomize](https://kustomize.io/) binary on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kustomize_version: v4.4.1
    kustomize_arch: amd64 # amd64, arm64, ppc64le or s390x
    setup_dir: /tmp
    kustomize_bin_path: /usr/local/bin/kustomize
    kustomize_repo_path: https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F{{ kustomize_version }}

This role always installs the latest version. See [available kustomize releases](https://github.com/kubernetes-sigs/kustomize/releases).

    kustomize_version: v4.4.1

The path which kustomize will be downloaded.

    kustomize_repo_path: https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F{{ kustomize_version }}

The location where the kustomize binary will be installed.

    kustomize_bin_path: /usr/local/bin/kustomize

kustomize supports amd64, arm64, ppc64le and s390x CPU architectures, just change for the main architecture of your CPU.

    kustomize_arch: amd64 # amd64, arm64, ppc64le or s390x

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kustomize

## License

MIT / BSD
